import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Header from './components/Header'
import MainLayout from "./layouts/MainLayout";
import Button from "./components/Button";

function State() {
  return (
    <div className="State mt-5">
      <MainLayout>
      <Header title="Button Components" />
        <Button variant="primary">
          Click Here
        </Button>

        <Button variant="secondary">
          Click Here
        </Button>

        <Button variant="black">
          Click Here
        </Button>
      </MainLayout>
    </div>
  );
}

export default State;
