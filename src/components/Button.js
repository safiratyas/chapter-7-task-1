import React from 'react';
import classNames from "classnames"
import { PropTypes } from "prop-types"
import style from "./Button.module.css"

const backgroundColor = {
  primary: "#CD5C5C",
  secondary: "#1DB954",
  black: "#DFFF00",
}

const textColor = {
  primary: "white",
  secondary: "white",
  black: "#ffffff",
}

const Button = ({ children, variant, onClick, className, ...props }) => {
  const styles = {
    backgroundColor: backgroundColor[variant] || backgroundColor.primary,
    color: textColor[variant] || textColor.primary,
  };

  return (
    <button style={styles} {...props} onClick={onClick} className={classNames(style.Button, className)}>
      {children}
    </button>
  )
}

Button.propTypes = {
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "black"
  ]),
  onClick: PropTypes.func,
}

Button.defaultProps = {
  variant: "primary",
  onClick: () => {},
}

export default Button;
