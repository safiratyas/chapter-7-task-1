import React from "react";
import Header from "./Header";
import Button from "./Button";
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import MainLayout from "../layouts/MainLayout";

const Card = (props) => {
  const { title, description, imgSrc, imgAlt, btnPrimary, btnSecondary} = props;
  return (
          <div className="card" style={{ width: "100%" }}>
            <MainLayout>
            <Header title="Songs"/>
            <img src={imgSrc} className="card-img-top" alt={imgAlt} />
            <div className="card-body">
              <h5 className="card-title">{title}</h5>
              <p className="card-text">{description}</p>
            </div>
            <Row style={{ marginBottom:"10%" }}>
              <Col>
              <Button variant="primary">{btnPrimary}</Button>
              </Col>
              <Col>
              <Button variant="secondary">{btnSecondary}</Button>
              </Col>
            </Row>
            {/* <Button variant="secondary">{btnSecondary}</Button>
            <Button variant="black">{btnBlack}</Button> */}
            </MainLayout>
          </div>

  )
}
export default Card;
