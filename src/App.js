import Card from './components/Card';
import Header from './components/Header';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import MainLayout from './layouts/MainLayout';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

function App() {
  return (
      <div className="App">
            <MainLayout>
            <Header
            title="Hello!"
            />
            <Container>
              <Row>
                <Col>
                  <Card
                    title="BTS"
                    description="Lorem ipsum dolor sit amet"
                    btnPrimary="iTunes"
                    btnSecondary="Spotify"
                    imgSrc="https://placeimg.com/320/240/any"
                    imgAlt="Hello"
                  />
                </Col>
                <Col>
                  <Card
                    title="Taylor Swift"
                    description="Lorem ipsum dolor sit amet"
                    btnPrimary="iTunes"
                    btnSecondary="Spotify"
                    imgSrc="https://placeimg.com/320/240/any"
                    imgAlt="Hello"
                  />
                </Col>
                <Col>
                  <Card
                    title="Billie Eilish"
                    description="Lorem ipsum dolor sit amet"
                    btnPrimary="iTunes"
                    btnSecondary="Spotify"
                    imgSrc="https://placeimg.com/320/240/any"
                    imgAlt="Hello"
                  />
                </Col>
              </Row>
            </Container>
          </MainLayout>
      </div>
      
  );
}


export default App;
